﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using SwarmClient.XInput;
using SwarmCore.Values;

namespace SwarmClient
{
	public class SwarmClientApp
	{
		private static int TargetTickrate = 16; // Target send rate in ms.

		private static readonly ManualResetEvent Exit = new ManualResetEvent(false);

		public static void Main(string[] args) {
			Console.WriteLine("Enter an ip:port to connect to (ex: 1.2.3.4:8090). Empty for controller test mode.");
			string connectTo = Console.ReadLine();

			Console.WriteLine("Enter a tickrate in mx (ex: 16). Empty for default (16).");
			var tickRate = Console.ReadLine();
			if (string.IsNullOrWhiteSpace(tickRate) || !int.TryParse(tickRate, out TargetTickrate)) {
				TargetTickrate = 16;
			}

			if (!string.IsNullOrWhiteSpace(connectTo)) {
				StartServer(connectTo);
				Console.WriteLine($"Connecting to {connectTo}");
			} else {
				Console.WriteLine("Starting in test mode (controller input monitoring only).");
			}
			
			Thread.Sleep(500);
			Task.Factory.StartNew(StartControllerMonitor);

			Exit.WaitOne();
		}

		private static Socket client;
		private static XInputState State = new XInputState();
		private static readonly Stopwatch Timing = new Stopwatch();

		private static readonly byte[] SendBuffer = new byte[16];

		static void StartServer(string connectTo) {
			var addressParts = connectTo.Split(new[] {":"}, StringSplitOptions.RemoveEmptyEntries);
			client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			client.Connect(IPAddress.Parse(addressParts[0]), Convert.ToInt32(addressParts[1]));
			Console.WriteLine("Socket Connected");
		}

		static void StartControllerMonitor() {
			Console.WriteLine("Client::Starting controller monitor...");

			// Initialize sendBuffer with SwarmPad preamble.
			Array.Copy(SwarmConstants.PacketPreamble, 0, SendBuffer, 0, SwarmConstants.PacketPreamble.Length);

			while (true) {
				Timing.Start();
				DoControllerProcess();

				while(Timing.ElapsedMilliseconds < TargetTickrate) { // Semi-busy loop.
					if ((TargetTickrate - Timing.ElapsedMilliseconds) > 2) {
						Thread.Sleep(Convert.ToInt32(TargetTickrate - Timing.ElapsedMilliseconds) - 1);
					}
				}

				Timing.Reset();
				Timing.Stop();
			}
		}

		static void DoControllerProcess() {
			NativeMethods.XInputGetState(0, ref State);
			
			if (client != null) { // Mimic XInputState arrangement. Buttons LT RT LX LY RX RY
				Array.Copy(BitConverter.GetBytes(State.Gamepad.Buttons), 0, SendBuffer, 4, 2);
				SendBuffer[6] = State.Gamepad.LeftTrigger;
				SendBuffer[7] = State.Gamepad.RightTrigger;
				Array.Copy(BitConverter.GetBytes(State.Gamepad.ThumbLX), 0, SendBuffer, 8, 2);
				Array.Copy(BitConverter.GetBytes(State.Gamepad.ThumbLY), 0, SendBuffer, 10, 2);
				Array.Copy(BitConverter.GetBytes(State.Gamepad.ThumbRX), 0, SendBuffer, 12, 2);
				Array.Copy(BitConverter.GetBytes(State.Gamepad.ThumbRY), 0, SendBuffer, 14, 2);
				client.Send(SendBuffer);
			}
			
			//Console.WriteLine($"{State.Gamepad.ThumbLX}, {State.Gamepad.ThumbLY}");
		}

	}
}
