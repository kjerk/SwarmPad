﻿namespace SwarmCore.Values
{
	public struct XInputButtons
	{
		public const ushort DPAD_UP = 0x0001;
		public const ushort DPAD_DOWN = 0x0002;
		public const ushort DPAD_LEFT = 0x0004;
		public const ushort DPAD_RIGHT = 0x0008;
		public const ushort START = 0x0010;
		public const ushort BACK = 0x0020;
		public const ushort LEFT_THUMB = 0x0040;
		public const ushort RIGHT_THUMB = 0x0080;
		public const ushort LEFT_SHOULDER = 0x0100;
		public const ushort RIGHT_SHOULDER = 0x0200;
		public const ushort A = 0x1000;
		public const ushort B = 0x2000;
		public const ushort X = 0x4000;
		public const ushort Y = 0x8000;

		public const ushort START_OFF = 0xFFEF;

		public const ushort A_OFFMASK = 0xEFFF;
		public const ushort B_OFFMASK = 0xDFFF;
		public const ushort X_OFFMASK = 0xBFFF;
		public const ushort Y_OFFMASK = 0x7FFF;
	}
}
