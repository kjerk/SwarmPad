﻿using System.Runtime.InteropServices;

namespace SwarmCore.Model
{
	//[StructLayout(LayoutKind.Explicit)]
	[StructLayout(LayoutKind.Sequential)]
	public class XInputGamepad
	{
		[MarshalAs(UnmanagedType.I2)]
		//[FieldOffset(0)]
		public ushort wButtons;

		[MarshalAs(UnmanagedType.I1)]
		//[FieldOffset(2)]
		public byte bLeftTrigger;

		[MarshalAs(UnmanagedType.I1)]
		//[FieldOffset(3)]
		public byte bRightTrigger;

		[MarshalAs(UnmanagedType.I2)]
		//[FieldOffset(4)]
		public short sThumbLX;

		[MarshalAs(UnmanagedType.I2)]
		//[FieldOffset(6)]
		public short sThumbLY;

		[MarshalAs(UnmanagedType.I2)]
		//[FieldOffset(8)]
		public short sThumbRX;

		[MarshalAs(UnmanagedType.I2)]
		//[FieldOffset(10)]
		public short sThumbRY;

		public bool IsButtonPressed(int buttonFlags) {
			return (wButtons & buttonFlags) == buttonFlags;
		}

		public bool IsButtonPresent(int buttonFlags) {
			return (wButtons & buttonFlags) == buttonFlags;
		}

		public void Copy(XInputGamepad source) {
			sThumbLX = source.sThumbLX;
			sThumbLY = source.sThumbLY;
			sThumbRX = source.sThumbRX;
			sThumbRY = source.sThumbRY;
			bLeftTrigger = source.bLeftTrigger;
			bRightTrigger = source.bRightTrigger;
			wButtons = source.wButtons;
		}

		public override bool Equals(object obj) {
			if (ReferenceEquals(null, obj)) return false;
			return obj is XInputGamepad && Equals((XInputGamepad) obj);
		}

		public bool Equals(XInputGamepad other) {
			return wButtons == other.wButtons && bLeftTrigger == other.bLeftTrigger && bRightTrigger == other.bRightTrigger && sThumbLX == other.sThumbLX && sThumbLY == other.sThumbLY && sThumbRX == other.sThumbRX && sThumbRY == other.sThumbRY;
		}

		public override int GetHashCode() {
			unchecked {
				var hashCode = wButtons.GetHashCode();
				hashCode = (hashCode * 397) ^ bLeftTrigger.GetHashCode();
				hashCode = (hashCode * 397) ^ bRightTrigger.GetHashCode();
				hashCode = (hashCode * 397) ^ sThumbLX.GetHashCode();
				hashCode = (hashCode * 397) ^ sThumbLY.GetHashCode();
				hashCode = (hashCode * 397) ^ sThumbRX.GetHashCode();
				hashCode = (hashCode * 397) ^ sThumbRY.GetHashCode();
				return hashCode;
			}
		}
	}
}
