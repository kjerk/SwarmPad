﻿using System;
using System.Runtime.InteropServices;

namespace SwarmCore.Model
{
	//[StructLayout(LayoutKind.Explicit)]
	[StructLayout(LayoutKind.Sequential)]
	public class XInputState
	{
		//[FieldOffset(0)]
		public int PacketNumber;

		//[FieldOffset(4)]
		public XInputGamepad Gamepad;

		public void Copy(XInputState source) {
			PacketNumber = source.PacketNumber;
			Gamepad.Copy(source.Gamepad);
		}

		public override bool Equals(object obj) {
			if (ReferenceEquals(null, obj)) return false;
			return obj is XInputState && Equals((XInputState) obj);
		}

		public bool Equals(XInputState other) {
			return PacketNumber == other.PacketNumber && Gamepad.Equals(other.Gamepad);
		}

		public override int GetHashCode() {
			unchecked {
				return (PacketNumber * 397) ^ Gamepad.GetHashCode();
			}
		}

		public override string ToString() {
			return $"State: BT{Convert.ToString(Gamepad.wButtons, 2).PadLeft(16)} LT:{Gamepad.bLeftTrigger} RT:{Gamepad.bRightTrigger} XYXY:" +
			       $"{Gamepad.sThumbLX}|{Gamepad.sThumbLY}|{Gamepad.sThumbRX}|{Gamepad.sThumbRY}";
		}
	}
}
