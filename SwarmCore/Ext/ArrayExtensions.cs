﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SwarmCore.Ext
{
	public static class ArrayExtensions
	{
		public static List<int> IndexOfSequence(this byte[] buffer, byte[] pattern, int startIndex) {
			List<int> positions = new List<int>();
			int i = Array.IndexOf<byte>(buffer, pattern[0], startIndex);
			while (i >= 0 && i <= buffer.Length - pattern.Length) {
				byte[] segment = new byte[pattern.Length];
				Buffer.BlockCopy(buffer, i, segment, 0, pattern.Length);
				if (segment.SequenceEqual<byte>(pattern))
					positions.Add(i);
				i = Array.IndexOf<byte>(buffer, pattern[0], i + pattern.Length);
			}

			return positions;
		}

		public static int Search(this byte[] haystack, byte[] needle, int ceiling = -1) {
			int[] lookup = new int[256];
			for (int i = 0; i < lookup.Length; i++) { lookup[i] = needle.Length; }

			for (int i = 0; i < needle.Length; i++) {
				lookup[needle[i]] = needle.Length - i - 1;
			}

			int index = needle.Length - 1;
			var lastByte = needle.Last();
			int ceil = ceiling != -1 ? ceiling : haystack.Length;
			while (index < ceil) {
				var checkByte = haystack[index];
				if (haystack[index] == lastByte) {
					bool found = true;
					for (int j = needle.Length - 2; j >= 0; j--) {
						if (haystack[index - needle.Length + j + 1] != needle[j]) {
							found = false;
							break;
						}
					}

					if (found)
						return index - needle.Length + 1;
					else
						index++;
				} else {
					index += lookup[checkByte];
				}
			}

			return -1;
		}
	}
}
