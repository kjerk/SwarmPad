## Swarmpad ##

### tl;dr ###

Swarmpad enables remote play of a game by hooking a game process and allowing input to come from across the network via UDP.

### Elevator Pitch ###

Swarmpad is a controller utility and social gaming experiment that asks the question "Can humans actually cooperate? What if there's loot involved?".

This is inspired partly by the 'Twitch Plays' idea, and extends it to several cooperative playing ideas. If two players were both controlling one character and their controls were averaged, can they achieve a hivemind (Swarm :D) and progress through a game. What about four players? What about 8v8 on a LAN but only controlling two characters?

To this end, the 'Server' application of Swarmpad hooks a running game process, overriding the controller fetch data method, and can then arbitrarily change input. This is then hooked to a Server daemon and input can then come across the network from a client, or N clients, at which point the Mixing mode will determine what happens.

### Controller Mixing Modes ###
- [x] Override Mode (remote play, local controller disabled)
- [ ] Swarm Mode (Local and all remote controllers smartly averaged. 0 trigger and 255 trigger, 255 wins. 20 trigger and 40 trigger averaged to 30.)
- [ ] Mapped Mode (Override is mapped per-button/axis/trigger)
- [ ] Copilot Mode (Local controller priority, remote controller takes complete control on input but is given back on idle)
- [ ] Bureaucracy Mode (Local controller priority, N clients have input averaged and given as one input.)

### Todo ###

- [x] Initial Client, Server, and Core implementations.
- [x] Very accurate timing mechanism for client.
- [x] Basic packet structure.
- [x] Basic threading model. (Split between controller poll and send/recv).
- [ ] Gui. Launcher or otherwise.
- [ ] CLI rather than STDin for config. This would make launchers easier as well.
- [ ] Java version of client, or any cross platform. [Go](https://github.com/tajtiattila/xinput/blob/master/xinput.go)/.net Core/C++. Anything matching the packet descriptor.
- [ ] NAT traversal. [ICE](https://github.com/jitsi/ice4j)/[STUN](https://gist.github.com/yetithefoot/7592580)/TURN?

### Standing Ideas ###

- Node version of client (this is a great idea). Opens a chrome window to a page served by local node. Page is GUI, Node runs native code. Best of both worlds.
- Test ultra low latency video RTSP servers.
- WebRTC/Websocket communication in place of UDP, needs benchmarking, but opens up browsers as a DIRECT input method (Imagine being on the twitch page and being able to directly contribute with a controller.) ([Gamepad api](https://developer.mozilla.org/en-US/docs/Web/API/Gamepad_API#Browser_compatibility) is complete).
- Externalize packet description to something everything can integrate against regardless of language. Json Schema, XML, whatever.
- DirectX overlay showing controller input similar to [Gamepad Viewer](https://gamepadviewer.com), but take multiple controllers and Mixing Mode into account to show what's going on.
- Controller heatmap.
- Every low level server framework I looked at was garbage (Mina, Netty, Neutrino), but there might be some others out there that make it nicer than speaking raw socket.

### Authors ###
* **kjerk** - [Github](https://github.com/kjerk)
