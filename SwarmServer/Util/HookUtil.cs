﻿using System;
using Nektra.Deviare2;

namespace SwarmServer.Util
{
	public static class HookUtil
	{
		[MTAThread]
		public static NktProcess GetProcess(NktSpyMgr activeManager, string processName) {
			NktProcessesEnum enumProcess = activeManager.Processes();
			NktProcess tempProcess = enumProcess.First();
			while(tempProcess != null) {
				if(tempProcess.Name.Equals(processName, StringComparison.InvariantCultureIgnoreCase)) {
					return tempProcess;
				}
				tempProcess = enumProcess.Next();
			}
			return null;
		}
	}
}
