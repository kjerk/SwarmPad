﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Nektra.Deviare2;
using SwarmCore.Ext;
using SwarmCore.Model;
using SwarmCore.Values;
using SwarmServer.Util;

namespace SwarmServer
{
	public class SwarmServerApp
	{
		private static readonly int defaultPort = 8090;
		private static readonly ManualResetEvent ExitEvent = new ManualResetEvent(false);
		private static readonly NktSpyMgr Spy = new NktSpyMgr();
		
		private static XInputState GamePadState = new XInputState(); // Struct facade.
		private static volatile byte[] GamePadStateRaw = new byte[16]; // Underlying data for above struct, raw access.
		private static GCHandle PinnedAlloc; // Handle on above struct and raw data to keep pinned.
		private static MixingMode mixingMode = MixingMode.Override;

		public enum MixingMode
		{
			Override,
			Averaging
		}

		private static readonly byte[] P1InputBuffer = new byte[12];

		[MTAThread]
		public static void Main(string[] args) {
			Console.WriteLine("Please enter a port to expose for the server (ex: 8090)");
			string listenPort = Console.ReadLine();

			Console.WriteLine("Please enter the executable name to hook (ex: RocketLeague.exe), skip for controller server debug mode.");
			string programToHook = Console.ReadLine();

			Console.WriteLine("Please enter a mixing mode, 1 - Override, 2 - Averaging, blank for default (Override)");
			string inputMixingMode = Console.ReadLine();
			mixingMode = inputMixingMode == "1" ? MixingMode.Override : MixingMode.Averaging;

			Task.Factory.StartNew(() => {
				if(!int.TryParse(listenPort, out var listenPortInt)) {
					listenPortInt = defaultPort;
				}
				StartServer(listenPortInt);
			});

			PinnedAlloc = GCHandle.Alloc(GamePadStateRaw, GCHandleType.Pinned);

			if (!string.IsNullOrWhiteSpace(programToHook)) {
				Console.WriteLine("Server:: Hooking program.");
				Spy.Initialize();
				var ps = HookUtil.GetProcess(Spy, programToHook);
				NktHook hook = Spy.CreateHook("xinput1_3.dll!XInputGetState", (int)(eNktHookFlags.flgOnlyPostCall));

				Task.Factory.StartNew(() => {
					Console.WriteLine("Server::Beginning hook...");
					hook.OnFunctionCalled += onXInputGetState;
					hook.Hook(true);
					hook.Attach(ps, true);
					Console.WriteLine("Server:: Hooked program.");
				});
			}

			ExitEvent.WaitOne();
		}

		[MTAThread]
		private static void onXInputGetState(NktHook hook, NktProcess proc, NktHookCallInfo callinfo) {
			var args = callinfo.Params();
			var padState = args.GetAt(1);

			if(!padState.IsNullPointer) {
				switch (mixingMode) {
					case MixingMode.Averaging:
						Array.Copy(P1InputBuffer, 0, GamePadStateRaw, 4, 12);
						break;
					case MixingMode.Override:
					default:
						Array.Copy(P1InputBuffer, 0, GamePadStateRaw, 4, 12);
						break;
				}
				// Read pad state from arg1.
				//padState.Memory().ReadMem(PinnedAlloc.AddrOfPinnedObject(), padState.PointerVal, (IntPtr)16);
				//Marshal.PtrToStructure(PinnedAlloc.AddrOfPinnedObject(), GamePadState);

				// Overwrite the returning state of the call with GamePadStateRaw, which is updated asynchronously by the server.
				padState.Memory().WriteMem(padState.PointerVal, PinnedAlloc.AddrOfPinnedObject(), (IntPtr)16);
			}
		}

		private static volatile bool serverListen = true;

		public static void StartServer(int listenPort) {
			var exit = new ManualResetEvent(false);

			Task.Factory.StartNew(() => {
				byte[] buf = new byte[4096];
				int read = 0;

				byte[] controllerPacket = new byte[12];

				Debug.WriteLine("Server::Starting and binding.");

				using(var server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)) {
					server.Bind(new IPEndPoint(IPAddress.Any, 8090));
					Debug.WriteLine("Server::Started and bound, listening...");

					while(serverListen) {
						read = server.Receive(buf);

						if(read < 16) { // Not full packet.
							continue;
						}

						var packetStart = buf.Search(SwarmConstants.PacketPreamble);
						if(packetStart != -1) {
							Array.Copy(buf, packetStart + SwarmConstants.PacketPreamble.Length, P1InputBuffer, 0, controllerPacket.Length);
							//Marshal.PtrToStructure(PinnedAlloc.AddrOfPinnedObject(), GamePadState);
							// The above is copied into GamePadStateRaw skipping 0-3 in GamePadStateRaw which is taken up by an int for player index (see XInputState).
							//Console.WriteLine($"Server::{GamePadState}");
						}
					}
				}
			});

			exit.WaitOne();
		}
	}
}
