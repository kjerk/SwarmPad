﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SwarmCore.Ext;
using SwarmCore.Values;

namespace SwarmTests
{
	[TestClass]
	public class SocketTests
	{
		private static volatile bool serverListen = true;

		private static readonly ManualResetEvent Done = new ManualResetEvent(false);

		private readonly string[] _messages = {
			"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"
		};

		[TestMethod]
		public void TestClientServerInterop() {
			var tasks = new List<Task> {
				Task.Factory.StartNew(doServer)
			};

			Thread.Sleep(500);
			tasks.Add(Task.Factory.StartNew(doClient));
			Thread.Sleep(500);

			Task.WaitAll(tasks.ToArray());
		}

		private void doServer() {
			byte[] buf = new byte[4096];
			int read = 0;

			using (var server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)) {
				server.Bind(new IPEndPoint(IPAddress.Any, 8090));

				string message;

				while(serverListen) {
					read = server.Receive(buf);

					message = Encoding.ASCII.GetString(buf, 0, read);
					Console.WriteLine($"Server::{message}");
					if(message == "20") {
						break;
					}
				}
			}
			Console.WriteLine("Server::Complete");
		}

		private void doClient() {
			using (var client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)) {
				client.Connect(IPAddress.Loopback, 8090);

				foreach(var message in _messages) {
					client.Send(Encoding.ASCII.GetBytes(message));
				}
			}
			Console.WriteLine("Client::Complete");
		}


		[TestMethod]
		public void TestControllerServer() {
			var exit = new ManualResetEvent(false);

			short lx, ly, rx, ry, buttons;
			byte lt, rt;

			List<int> packetStarts;

			Task.Factory.StartNew(() => {
				byte[] buf = new byte[4096];
				int read = 0;

				byte[] controllerPacket = new byte[16];

				Debug.WriteLine("Server::Starting and binding.");

				using(var server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)) {
					server.Bind(new IPEndPoint(IPAddress.Any, 8090));
					Debug.WriteLine("Server::Started and bound, listening...");

					while(serverListen) {
						read = server.Receive(buf);

						if (read < 16) { // Not full packet.
							continue;
						}

						packetStarts = buf.IndexOfSequence(SwarmConstants.PacketPreamble, 0);
						if (packetStarts != null && packetStarts.Count > 0) {
							foreach (var packetStart in packetStarts) {
								Array.Copy(buf, 0, controllerPacket, packetStart, 16);
								(buttons, lt, rt, lx, ly, rx, ry) = (
									BitConverter.ToInt16(controllerPacket, 4),
									controllerPacket[14],
									controllerPacket[15],
									BitConverter.ToInt16(controllerPacket, 6),
									BitConverter.ToInt16(controllerPacket, 8),
									BitConverter.ToInt16(controllerPacket, 10),
									BitConverter.ToInt16(controllerPacket, 12)
								);

								Debug.WriteLine($"Server::LX:{lx} LY:{ly} RX:{rx} RY:{ry} LT:{lt} RT:{rt} Buttons: {Convert.ToString(buttons, 2)}");
							}
						}
					}
				}
			});

			exit.WaitOne();
		}
	}
}
